<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/styles.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@48,400,0,0" />
	<title>Market</title>
</head>
<body>
	<header></header>
	<nav class="topbar">
		<input type="text" placeholder="search.." class="searchbar">
		<button class="searchbutton">
			<a href="#"><span class="material-symbols-outlined , icons">search</span></a>
		</button>
		<a href="#"><span class="material-symbols-outlined , icons">contrast</span></a>
	</nav>
	<aside class="sidebar">
		<a href="#" title="Inicio"><span class="material-symbols-outlined">home</span></a>
		<a href="#" title="Paquetes"><span class="material-symbols-outlined">widgets</span></a>
		<a href="#" title="Contacto"><span class="material-symbols-outlined">call</span></a>
		<a href="#" title="Ayuda"><span class="material-symbols-outlined">help</span></a>
	</aside>
	<footer class="fbar">
		<div class="nbar">
			<h3>Información</h3>
		</div>
		<div class="nbar">
			<h3>Síguenos en</h3>
			<div class="sbar">
				<a href="#"><img class="sicons" src="media/img/png/facebook.png"></a>
				<a href="#"><img class="sicons" src="media/img/png/instagram.png"></a>
				<a href="#"><img class="sicons" src="media/img/png/telegrama.png"></a>
				<a href="#"><img class="sicons" src="media/img/png/whatsapp.png"></a>
				<a href="#"><img class="sicons" src="media/img/png/youtube.png"></a>
			</div>
		</div>
		<div class="nbar">
			<h3>Copyright © 2023 Nombre_Empresa.com.mx</h3>
			<div class="cbar">
				<a href="https://www.flaticon.es/iconos" title="www.flaticon.es">Iconos creados por Pixel perfect - Flaticon</a>
			</div>
	</footer>

</body>
</html>
